A write up of the 2018 Michigan voter file predictions are provided [here](https://gitlab.com/smidtc/2018predictions/blob/master/2018predictions.pdf)

A record of the predicted probability for all 7.4 million voters is provided [here](https://gitlab.com/smidtc/2018predictions/blob/master/individual_predictions_nonames.zip). Note that the data has no personal information except the State of Michigan's Voter ID number. 

A **summary assessment** of the 2018 predictions are provided in this [image of a poster](https://gitlab.com/smidtc/2018predictions/blob/master/finalposter.png) I presented at the 2019 Annual Meeting of the American Associate of Public Opinion Researchers. 