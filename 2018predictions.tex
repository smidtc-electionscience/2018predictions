\documentclass[12pt]{article}
\usepackage[english]{babel}

% Math and font
\usepackage{amsmath}
\usepackage{newtxtext}
\usepackage{newtxmath}

% Bibliography
\usepackage[backend=biber,bibencoding=utf8,notes,sorting=nyt,authordate,natbib]{biblatex-chicago}
\addbibresource{Master.bib}
%\usepackage[backend=biber,style=chicago-authordate,maxnames=3,natbib=true]{biblatex} 

%%% Document layout, margins
%\usepackage[margin=1in]{geometry}
\usepackage{afterpage}
\pdfpagewidth 8.5in
\pdfpageheight 11in
\usepackage{setspace}
\setlength{\footnotesep}{14pt}
\usepackage{geometry}
\geometry{ left = 1in, right = 1in, top = 1in, bottom = 1in }
\usepackage{graphicx}
\usepackage[small,compact]{titlesec} %\usepackage[small,compact]{titlesec} 
\setcounter{secnumdepth}{0}
\usepackage{appendix}

%% PDF Settings
\usepackage[bookmarks, colorlinks,breaklinks, pdftitle={},pdfauthor={}]{hyperref}
%\hypersetup{linkcolor=Black,citecolor=Black,filecolor=Black,urlcolor=Black}
\hypersetup{linkcolor=NavyBlue,citecolor=NavyBlue,filecolor=NavyBlue,urlcolor=NavyBlue}

% add packages
%% Tables & figures
\usepackage{booktabs}
\usepackage{dcolumn}
\newcolumntype{d}{D{.}{.}{1.5}}
\newcolumntype{.}{D{.}{.}{-1}}

%% Caption labels bold. Always left-align, do not center short ones.
%% Use . instead of : after label. Size option ,footnotesize.
\usepackage[bf,nooneline,labelsep=period]{caption}
\usepackage{wrapfig}  % wrappable figures
\usepackage{graphicx}
\usepackage{rotating}

%% Leftover
% Add quote environments
\usepackage[autostyle]{csquotes}
% More color options
\usepackage[dvipsnames,usenames]{xcolor}

%% New TX
\usepackage[T1]{fontenc}


\title{Turnout Predictions for Michigan Voters from the State Voterfile: General Election, November 2018}
\date{Final Report: \today}


\author{ Corwin D. Smidt\\Associate Professor\\Department of Political Science\\Michigan State University\\ \texttt{smidtc@msu.edu} \\\\ South Kedzie Hall\\368 Farm Lane, S303\\East Lansing, MI 48824\\517-353-3292}


\begin{document}

\maketitle

\section{Summary numbers}

\begin{itemize}
\item \textbf{Voterfile-based predicted turnout rate:} \textit{57.1\% among registered voters}, with a rough 90\% model-estimated range of 56.0-58.2\%.
\item \textbf{Expected number of voters:} \textit{4.24 million voters} and an 90\% model-based expected range of 4.16-4.33 million. But this prediciton excludes some newly registered voters that were yet included in the state voterfile.
\end{itemize}

These predictions are not meant to be a precise forecast, more so like a gauge or temperature of what turnout levels would likely be if past patterns hold. The model-estimated prediction errors are likely too small (we have 60 million observations, but only 5-6 elections in the data to learn how newly registered voters typically behave). Thus, based on past performance (again this is only over a small sample of elections) the 90\% outcome range of the registered voter turnout rate is more like 54.85 - 59.36.

Regardless, the expected turnout rate would be a new high when compared to recent past elections. 2006 was Michigan's most recent high turnout gubernatorial election. The registered voter turnout rate that year was 53.6\%. Michigan has not seen a registered voter turnout rate of 57\% in a midterm since 1978. These predictions seem reasonable, however, considering the \href{https://www.mlive.com/news/index.ssf/2018/08/michigan_voters_set_new_turnou.html}{record high turnout in the August primary}.

The predictions are more precise in telling us what the relative composition of the Michigan electorate will look like (i.e., how young it will be and from what areas). I go into more detail about these expectations below. But I will highlight the two summary trends at the outset:

\subsubsection{1. Demographic trends are toward a more purple state}

Despite higher than usual midterm turnout rates, the balance of voters across the state is shifting away from the typical high turnout Democratic Party advantage. This is largely because of the consistent decline of registered voters in Wayne County over the last 15 years.  Even with much higher turnout, Wayne County's is expected to make up a smaller proportion of Michigan voters than it did in 2016 or 2014.  When re-weighting county level results to their expected 2018 turnout, Trump's vote percentage would improve over a 2014 or 2016 turnout model. This does not mean Republicans will continue to win these counties, but it does mean demographic trends are pushing Michigan toward being more so a battleground.

\subsubsection{2. A relatively young midterm electorate is expected, but still older than 2016}

High turnout rates mean that there will be a younger midterm electorate than what Michigan saw in 2010 or 2014. However, we should still expect an older electorate than what we saw in 2016. Voters under 30 are expected to account for 9.8\% of the electorate. That's much higher than the 6\% rate we saw in 2014, but still noticeably less than the 13.6\% proportion of voters under 30 in 2016. Interestingly, it is not the elderly but the late middle age voters (45-59), who as a group are trending toward making up a smaller proportion of the electorate.

\section{Where does this prediction come from? An explanation and disclaimer}

The State of Michigan provides a public record of registered voters in Michigan and their turnout history in the State's Qualified Voter File (QVF). The data in this file include a registered voter's name, age, address, registration date (for resident's current jurisdiction), and voter history (for resident's current jurisdiction).

Knowing a person's age, registration date, and location provide only limited amounts of information for making a prediction. But knowing a person's voter history allow some ability to classify voters as to whether they are likely to vote or not for certain types of elections.   There remain some important qualifications to these predictions:
\begin{enumerate}
\item Most importantly, this is not a model or prediction of campaign-driven turnout! These predictions set a baseline expectation for what turnout should be given prior history of turnout for current voters and how that history matches with three election-type factors: 1) gubernatorial vs. presidential election cycle; 2) ballot competition (proportion of prominent offices with two-party competition); and 3) year-specific tides. There is no information about polls, spending, or interest in ballot proposals.

If turnout is higher or lower than what is predicted, then that is a good sign that the choices and campaigns either mobilized or dissuaded registered voters from turning out. Record high turnout in the August primary was somewhat expected because the primary had one of the highest level of ballot competition we have seen. The record high turnout is also, however, what this model uses to predict this year's election tide. Hence, these predictions of a high tide in turnout may be off to the degree the general election campaign does not match the intensity of the primary campaign. 
  
\item These predictions exclude additional voters that registered after October 2 and before the deadline of October 9. Since the Secretary of State doesn't collect these and make them fully available until the final week of the campaign, it would be too late to wait to predict based on this file. My version of the QVF includes 7,426,719 registered voters; the state lists about 35,000 more voters as officially registered (\href{https://www.michigan.gov/documents/sos/2018_Registered_Voter_Count_612843_7.pdf}{7,471,088}).

\item The precision and accuracy of the predictions vary by how long a voter has been registered. For instance, here is the distribution of predictions for registered voters who have a record tracking back to the 2008 election cycle.

 \begin{center}
\includegraphics[width=5.5in]{longtimevoters.png}
\end{center}

In comparison, recently registered voters without a history from two years ago (about 15\% of registered voters) are harder to predict and often have predictions that are less precise. Since we lack little information about whether these people voted at their past address, the prediction only relies on the individual's age and address, which are not strong predictors. Most recent registrants only have one observation (the August primary), it is hard for any model to decipher voter turnout probabilities based on limited attributes and only one prior observation. Note the distribution of turnout estimates for voters who have registered since 2016.

\begin{center}
\includegraphics[width=5.5in]{newvoters.png}
\end{center}

The estimates are only very confident for voters who turned out in August. This is because past history tells us it is rare to observe a voter in August who then does not turn out to vote in November. However, observing a single instance of not voting in August is not nearly as informative because that is typical, and there are usually many voters in November midterms who did not vote in August. Thus this group of voters has a predicted probability that is below 50\%, but expects about 25\% of them to turn out. In contrast, the prediction for new voters without a history is very broad and only based on the loosely predictive measures of age and voter precinct.
\end{enumerate}

\section{Changes in Registered Voters}

The registered voter file rarely shows clear changes. For instance, the median age of registered voters has been 51 in 2014, 50 in 2016, and 50 again in 2018. Further examinations of age fail to demonstrate any clear youth surges in voter registration beyond what we see every election year.

{
\begin{table}[!ht]
  \caption{The Five Largest County's Proportion of State's Registered Voters by Election}
\label{countyreg}
\begin{center}
\begin{tabular}{lddd}
  &  \multicolumn{3}{c}{Election Year} \\
  County & \multicolumn{1}{c}{2014} & \multicolumn{1}{c}{2016} & \multicolumn{1}{c}{2018} \\
  \toprule
  & & & \\
  Wayne  &            18.15   &   17.50   &      17.35 \\
  Oakland &           12.70   &   12.78    &     12.79 \\
  Macomb  &           8.39    &    8.43    &      8.42 \\
  Kent    &           5.89    &    6.06    &      6.13  \\
  Genesee  &          4.46   &     4.43   &       4.40 \\
          & & & \\ 
  \bottomrule\\
\end{tabular}
\end{center}
\end{table}
}
But there is one change in the Michigan electorate that has been clear from a simple examination of its registered voters: the consistent decline in Wayne County's voting power.  As shown in Table~\ref{countyreg}, Wayne County has seen a sharp decline of 0.8 percentage points in its large share of state voters over the last four years. That may not seem like a lot, but this is Michigan's largest county and one that votes predominately for Democratic candidates. Moreover, it is not like these voters are simply moving to the outer suburbs. Oakland and Macomb County are only keeping a steady pace in their relative size. Kent shows a gain of 0.2 percentage points, which is similar to the growth seen in Washtenaw and Ottawa Counties.

\subsection{Consequences for the 2018 Electorate: A move closer toward battleground}

These shifts in the relative size of a county's voters are more pronounced when we look into actual voter turnout. Table~\ref{countypower} lists the top gainers and loser in county voting power over the last four years.

\begin{table}[!ht]
  \caption{Top Gainers and Losers in County Voting Power (Percent of Michigan Electorate)}
\label{countypower}
\begin{center}
\begin{tabular}{ldddd}
  County Voting Power  &  \multicolumn{1}{c}{2014}   &  \multicolumn{1}{c}{2016}  &   \multicolumn{1}{c}{Pred. 2018}  &   \multicolumn{1}{c}{Change} \\
  \toprule
  \multicolumn{5}{l}{\textit{Top Gainers}} \\
Kent                   &  5.94\%  &   6.18\%   &   6.52\%  &    +.58 \\
Washtenaw              &  3.82\%  &   3.93\%   &   4.08\%  &    +.27 \\
  Ottawa                 &  2.82\%  &   2.97\%   &   3.08\%  &    +.26 \\
  \\
  \multicolumn{5}{l}{\textit{Top Losers}} \\
Wayne                  & 16.14\%  &   16.17\%   &        15.61\% &      -.53 \\
Saginaw                &  2.13\%  &    1.96\%   &        1.56\%  &    -.20 \\
  Genesee                &  4.16\%  &    4.08\%   &         3.98\%  &   -.18 \\
  \bottomrule \\
\end{tabular}
\end{center}
\end{table}

Although Kent has not gained an equal number of registered voters as Wayne, the voters it has gained are highly likely to turnout. Kent County's voting power is expected to increase from 5.94\% of the state electorate in 2014 to 6.52\% of the state electorate in 2018. That .56 point growth rate outpaces Wayne County's decline of .53 percentage points.

Despite expectations of record high turnout, that growth in turnout is not expected to occur uniformally. Indeed, except for Washtenaw, the growth in voting power is largely in Republican leaning counties (Kent, Ottawa, Livingston), whereas the decline is largely in three of the more Democratic leaning counties in the state.

{
\begin{table}[!ht]
  \caption{Reweighting Republican Vote Percentages by Relative County Turnout}
\label{trumpvote}
\begin{center}
\begin{tabular}{lddd}
  &  \multicolumn{3}{c}{Election Year Turnout} \\
  Candidate & \multicolumn{1}{c}{2014} & \multicolumn{1}{c}{2016} & \multicolumn{1}{c}{Pred. 2018} \\
  \toprule
  Trump   &          47.65   &   47.59  &    47.68 \\
  Romney  &          45.02   &   45.03  &    45.19 \\
  \bottomrule\\
\end{tabular}
\end{center}
\end{table}
}
Table~\ref{trumpvote} shows the net consequence of this growth by reweighting county vote percentages for Trump and Romney by relative voting power in 2014, 2016, and what is predicted for 2018. Not surprisingly, Trump would have done slightly better in 2016 if county turnout matched the low turnout 2014 election. However, even with the record midterm turnout expected in 2018, the estimates suggest relatively greater growth in counties that supported Trump. A similar pattern emerges when looking at Romney support in a county.

To be clear, these marginal changes are small compared to the variations in party performance each cycle caused by its nominees and other election-year forces. These other forces would account for expectations of big Democratic wins in the state. Nevertheless, they signal a gradual change in Michigan toward being more of a battleground for intense state-wide election rather than a ``lean blue'' electorate.

\begin{center}
\includegraphics[width=5.5in]{waynecounty.png}
\end{center}
As already noted, it is easy to identify the main reason for this trend toward Michigan moving closer to party equity in statewide voting. As shown in the figure above, the heart of Democratic support in Michigan rests in Wayne County (with over 70\% of its voters usually supporting the Democratic Party) , but since the early 2000s Wayne County has seen a steady decline in its outsized influence within the state.

These county level estimates are available for download here: \href{https://gitlab.com/smidtc/2018predictions/blob/master/countlevelresults.csv}{https://gitlab.com/smidtc/2018predictions/blob/master/countlevelresults.csv}

\section{Perhaps a Surge in Young Midterm Voters}

{
\begin{table}[!ht]
  \caption{Percent of Michigan Electorate by Age Group}
\label{agevote}
\begin{center}
\begin{tabular}{lddd}
  &  \multicolumn{3}{c}{Election Year} \\
  Age Group & \multicolumn{1}{c}{2014} & \multicolumn{1}{c}{2016} & \multicolumn{1}{c}{Pred. 2018} \\
  \toprule
  18-29  &          6.0   &   13.6  &    9.8 \\
  30-44  &          16.8  &   21.3  &    19.8 \\
  45-59  &          30.6  &   29.1  &    27.9 \\
  60 and up &       46.6  &   36.1  &    42.5 \\
  & & & \\
  Median  &  \multicolumn{1}{c}{58}  & \multicolumn{1}{c}{53}  & \multicolumn{1}{c}{56} \\
  \bottomrule\\
\end{tabular}
\end{center}
\end{table}
}

Beyond location, the Michigan QVF only contains information about a registrant's age. Table~\ref{agevote} displays the expected age composition of the 2018 electorate as compared to state records for 2014 and 2016. 2014 was a low turnout election and young people were especially unlikely to vote. People under 30 only comprised 6\% of Michigan voters, wheras people 60 and over accounted for nearly 47\% of the electorate. In 2016, young voters more than doubled their relative composition of the Michigan electorate, making up 13.6\%. And we see the sharpest decline in relative size among those who were 60 or older at the time, down to 36.1\%.

The expectation for 2018, however, is a movement back towards 2014, but only halfway. Young people are expected to account for 9.8\% of Michigan voters. That's nearly the midpoint between the rates seen in 2014 and 2016.  Likewise, we should see a increase in voters 60 years or older such that they account for 42.5\% of all Michigan voters. But this gain in voting power for this group is primarily from a decline in the number of voters with ages between 45-59. There is a steady decline in the relative size of this group (from 30.6 to 29.1 and an expected 27.9). Indeed, the predictions anticipate that people under the age of 45 in 2018 will make up 29.6\% of Michigan's voter, which would be a not inconsequential gain of 6.8 percentage points from the 22.8\% they comprised in 2014, and a smaller 5.3 point drop from 2016.

These estimates may be too optimistic on a youth surge. It is possible that the absence of the relatively youth focused campaign of Abdul El-Sayed in the Democratic primary contest for Governor explains their relatively higher rates of August voting. His loss might mean that there is less youth interest in the November election. But the primary to general drop off rate has been small in past elections. Likewise, the pathways El-Sayed's campaign made in generating turnout in August are could easily hold considering there are at least three competitive U.S. House districts and some progressive-themed ballot initiatives.

\section{Estimation Details}

Define $y_{ijt}$ as the set of QVF observations of whether a registered voter $i$ in precinct $j$ turned out to vote in election $t$. Voter probabilities of turning out to vote are then modeled as follows:
\[ \Pr[y_{ijt} = 1] = \Lambda(x_{it}'\beta + z_{t}'\delta + Aug_t(\gamma_{1} + \zeta_{1j} + \nu_{1i}) + Competition_t(\gamma_{2} + \nu_{2i}) + \zeta_{0j} + \nu_{0i} ) \]
where
\begin{itemize}
\item $x_{it}$ are of set time-varying individual level predictors: logarithm of age;the interaction of log age with August primaries; the interaction of log age with the midterm cycle; a logarithm of month since registration; and dummies for an initial observation, whether a voter had recently registered in that quarter, and whether that recent regisration was during a presidential cycle.
\item $z_{t}$ are a set of election-specific indicators: whether the contest is a presidential primary; whether that election is during a midterm cycle; and the year of the contest.
\item $Aug_t$ is an indicator of whether the election is an August primary. The relationship between this variable and a voter turnout is allowed to vary at the precinct level and at the individual-level ($\zeta_{1j} + \nu_{1i}$). In other words, August turnout is assumed typically higher or lower in some precincts and among some individuals for factors beyond what is in the model. This specification essentially allows some individuals and precincts to be high turnout precincts in November but not in August (and vice-versa).
\item $Competition_t$ is a (logged) measure counting statewide and U.S. House contests with major party competition or, for August, with primary competition. This effect is allowed to vary by individual ($\nu_{2i}$). In short, it allows that some people show up to vote regardless of the amount of competition on the ballot. Whereas other people's likelihood of voting is responsive to the amount of competition on the ballot.
\item $\zeta_{0j}$ is a precinct-specific, time constant parameter that captures that precinct's unexplained tendency to have higher or lower registered voter turnout rates.
\item $\nu_{0i}$ is an individual-specific, time constant parameter that captures that individual's unexplained tendency to have higher or lower voter turnout rates.
\end{itemize}

\subsubsection{Model estimation and Predictions}

Multilevel logit model estimates were performed over 20 separate subsamples of 100 randomly sampled precincts (weighted by population) with up to 50 randomly sampled voters within each precinct. Since the average voter has a voter history spanning back ten past contests. There are about 50,000 observations for each of the 20 estimates. These parameters are then averaged and used to generate Empirical Bayes estimates of the precinct and individual specific error components ($\zeta, \nu$), using 5 integration points. These Emperical Bayes estimates are then combined with the model's fixed portion to generate a prediction of each voter's probability of turning out to vote in November.

\end{document}