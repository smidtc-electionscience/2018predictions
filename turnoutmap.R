rm(list=ls())
library(foreign)
library(ggmap)
#devtools::install_github("dkahle/ggmap")
library(scales)


statadata <- read.dta("precinctcount.dta")

mimap <- qmplot(lon,lat,colour=meanrate,alpha=I(.5),data=statadata, maptype="toner-lite",geom="point") + scale_colour_gradient("Exp. Precint Turnout Rate",low = "blue4", high="red4", guide = "colourbar", aesthetics = "colour")

mimap

ggsave("turnout.png")
